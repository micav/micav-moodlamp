# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  | 20+ hodin                       |
| odkud jsem čerpal inspiraci                   | https://www.instructables.com/RGB-Lamp-WiFi/ |
| odkaz na video                                | https://gitlab.spseplzen.cz/micav/micav-moodlamp/-/raw/main/dokumentace/video/moodlampV2.mp4 |
| jak se mi to podařilo rozplánovat             | Lepší jak v minulých projektech |
| proč jsem zvolil tento design                 | Byl to nejjednodušší design, stačilo postupovat dle návodu |
| zapojení                                      | https://gitlab.spseplzen.cz/micav/micav-moodlamp/-/raw/main/dokumentace/schema/moodlamp.png          |
| z jakých součástí se zapojení skládá          | Wemos D1 mini pro, 144led/metr WS2812B, DHT11, TP4056, přepínač, Li-On akumulátor, 100uF Capacitor, dráty, Filament |
| realizace                                     | https://gitlab.spseplzen.cz/micav/micav-moodlamp/-/raw/main/dokumentace/fotky/IMG_3172.jpg |
| UI                                            | http://10.202.31.129:1880/ui |
| co se mi povedlo                              | 3D tisk..                       |
| co se mi nepovedlo/příště bych udělal/a jinak | OTA                       |
| zhodnocení celé tvorby (návrh známky)         | Hodlám ve svém volném čase projekt zlepšit. 2? |