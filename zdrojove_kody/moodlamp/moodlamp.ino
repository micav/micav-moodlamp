//MQTT knihovny
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//NeoPixel knihovny
#include <Adafruit_NeoPixel.h>

//DHT knihovny
#include "DHT.h"

//Nastavení WiFi
const char* ssid = "dave-ubuntu";
const char* password = "18922981";

//Nastavení MQTT
const char* mqtt_server = "broker.hivemq.com";
const int mqtt_port = 1883;

//Nastavení NeoPixel
#define pinDIN D3
#define pocetLED 136
Adafruit_NeoPixel ws2812b = Adafruit_NeoPixel(pocetLED, pinDIN, NEO_GRB + NEO_KHZ800);

//Nastavení DHT
#define pinDHT D7
#define typDHT DHT11
DHT mojeDHT(pinDHT, typDHT);
unsigned long lastMsg = 0;

//Instance WiFi klienta a MQTT klienta
WiFiClient espClient;
PubSubClient client(espClient);

//------------------------------------------------------------------------------
//Moje metody
void readTemperatureAndHumidity(){
  float tep = mojeDHT.readTemperature();
  float vlh = mojeDHT.readHumidity();
  if (isnan(tep) || isnan(vlh)) {
    Serial.println("Chyba při čtení z DHT senzoru!");
  } else {
    Serial.print("Teplota: "); 
    Serial.print(tep);
    Serial.print(" stupnu Celsia, ");
    Serial.print("vlhkost: "); 
    Serial.print(vlh);
    Serial.println("  %");

    String tepStr = String(tep, 2);
    String vlhStr = String(vlh, 2);
    client.publish("micav/moodlamp/output/teplota", tepStr.c_str());
    client.publish("micav/moodlamp/output/vlhkost", vlhStr.c_str());
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  String dekoded;
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i=0;i<length;i++) {
    dekoded += (char)payload[i];
  }
  Serial.println(dekoded);

  if(strcmp(topic,"micav/moodlamp/input/efekty") == 0 ){/*
    if(strcmp(dekoded,"0")){
      uint32_t barva;
      barva = ws2812b.Color(0, 0, 0);
      for(int i = 0; i<=135; i++){
        ws2812b.setPixelColor(i, barva);
        delay(5);
      }
      ws2812b.show();
      Serial.println("Vypnuto.");
    }
    if(strcmp(dekoded,1)){
      uint32_t barva;
      //
      for(int j = 0; j <10; j++){
        barva = ws2812b.Color(226, 128, 23);  //oranžová
        for(int i = 0; i<=135; i++){
          ws2812b.setPixelColor(i, barva);
        }
        ws2812b.show();

        delay(200);

        barva = ws2812b.Color(0, 0, 0);  //off
        for(int i = 0; i<=135; i++){
          ws2812b.setPixelColor(i, barva);
        }
        ws2812b.show();

        delay(200);
      }
    
    }
    if(strcmp(dekoded,"1")){
      uint32_t barva;
      //
      for(int j = 0; j <10; j++){
        barva = ws2812b.Color(226, 128, 23);  //oranžová
        for(int i = 0; i<=135; i++){
          ws2812b.setPixelColor(i, barva);
        }
        ws2812b.show();

        delay(200);

        barva = ws2812b.Color(0, 0, 0);  //off
        for(int i = 0; i<=135; i++){
          ws2812b.setPixelColor(i, barva);
        }
        ws2812b.show();
      }
      
    }*/
  }
  if(strcmp(topic,"micav/moodlamp/input/rgb") == 0 ){
    rgb(dekoded);
  }
}

void rgb(String inputString){
  //začátek a konec čísel v řetězci
  int startIndex = inputString.indexOf("(") + 1;
  int endIndex = inputString.indexOf(")");

  //podřetězce obsahující čísla
  String numbers = inputString.substring(startIndex, endIndex);

  //funkce strtok k rozdělení řetězce na čísla podle čárky
  char *token = strtok(const_cast<char *>(numbers.c_str()), ",");
  int red = atoi(token);
  token = strtok(nullptr, ",");
  int green = atoi(token);
  token = strtok(nullptr, ",");
  int blue = atoi(token);

  //hodnoty r, g a b
  Serial.print("Red: ");
  Serial.println(red);
  Serial.print("Green: ");
  Serial.println(green);
  Serial.print("Blue: ");
  Serial.println(blue);

  //Nastav rgb na ws2812b
  uint32_t barva;
  barva = ws2812b.Color(red, green, blue);
  for(int i = 0; i<=135; i++){
    ws2812b.setPixelColor(i, barva);
    delay(5);
  }
  ws2812b.show();
  Serial.println("Barva nastavena.");
}


//------------------------------------------------------------------------------
//Metody k připojení

void connectWifi() {
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(250);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Připojeno k WiFi");
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // ... and resubscribe
      client.publish("micav/moodlamp/status", "Moodlamp zapnuta");
      client.subscribe("micav/moodlamp/input/efekty");
      client.subscribe("micav/moodlamp/input/rgb");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//------------------------------------------------------------------------------
//Setup a loop

void setup() {
  // Spuštění sériové komunikace
  Serial.begin(115200);
  ws2812b.begin();
  ws2812b.show();
  mojeDHT.begin();

  connectWifi();

  // Nastavení serveru a portu pro MQTT
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
}

void loop() {
  // Připojení k MQTT serveru
  if (client.state() != MQTT_CONNECTED) {
    reconnect();
  }else{
    client.loop();

    unsigned long now2 = millis();
    if(now2 - lastMsg > 2000){
      lastMsg = now2;
      readTemperatureAndHumidity();
    }
  }
}